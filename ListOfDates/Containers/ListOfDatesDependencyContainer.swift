//
//  ListOfDatesDependencyContainer.swift
//  ListOfDates
//
//  Created by Yauheni Zhurauski on 24.07.23.
//

import Foundation
import ListOfDatesIOS

final class ListOfDatesDependencyContainer {
    
    public func makeMainViewController() -> MainViewController {
        let mainViewModel = MainViewModel()
        
        let datesListViewControllerFactory = {
            self.createDatesListViewController()
        }
        
        
        let mainViewController = MainViewController(viewModel: mainViewModel, datesListViewControllerFactory: datesListViewControllerFactory)
        
        return mainViewController
        
    }
    
    private func createDatesListViewController() -> DatesListViewController {
        let buttonPositionManager = UserDefaultButtonPositionManager()
        return DatesListComposer().composeWith(buttonPositionGetter: buttonPositionManager, buttonPositionSaver: buttonPositionManager)
    }
    
}
