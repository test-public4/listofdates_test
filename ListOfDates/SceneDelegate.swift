//
//  SceneDelegate.swift
//  ListOfDates
//
//  Created by Yauheni Zhurauski on 24.07.23.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    let appContainer = ListOfDatesDependencyContainer()

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        window = UIWindow(windowScene: windowScene)
        window?.rootViewController = appContainer.makeMainViewController()
        window?.windowScene = windowScene
        window?.makeKeyAndVisible()
    }
}

