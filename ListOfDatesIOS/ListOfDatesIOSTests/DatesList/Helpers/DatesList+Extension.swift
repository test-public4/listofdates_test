//
//  DatesList+Extension.swift
//  ListOfDatesIOSTests
//
//  Created by Yauheni Zhurauski on 24.07.23.
//

import ListOfDatesIOS
import UIKit

extension DatesListViewController {
    
    var datesCount: Int? {
        let dataSource = contentView.datesListTableView.dataSource
        return dataSource?.tableView(contentView.datesListTableView, numberOfRowsInSection: 0)
    }
    
    var addButtonFrame: CGRect {
        contentView.addButton.frame
    }
    
    func simulateAddButtonTap() {
        contentView.addButton.simulateTap()
    }
    
}
