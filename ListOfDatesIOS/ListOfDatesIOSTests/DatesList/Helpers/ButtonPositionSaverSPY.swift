//
//  ButtonPositionSaverSPY.swift
//  ListOfDatesIOSTests
//
//  Created by Yauheni Zhurauski on 24.07.23.
//

import Foundation
import ListOfDatesIOS

final class ButtonPositionSaverSPY: ButtonPositionSaver {
    
    var requestCount: Int {
        saveButtonPositionCompletions.count
    }
    
    private var saveButtonPositionCompletions: [((Error?) -> Void)] = []
    
    func saveButtonPosition(at position: CGRect, name: String, completion: @escaping (Error?) -> Void) {
        saveButtonPositionCompletions.append(completion)
    }
    
    func completeSaveButtonPosition(with error: Error, at index: Int = 0) {
        saveButtonPositionCompletions[index](error)
    }
    
    func completeSaveButtonPositionSuccess(at index: Int = 0) {
        saveButtonPositionCompletions[index](nil)
    }
}
