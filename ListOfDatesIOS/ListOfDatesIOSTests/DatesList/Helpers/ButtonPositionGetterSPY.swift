//
//  ButtonPositionGetterSPY.swift
//  ListOfDatesIOSTests
//
//  Created by Yauheni Zhurauski on 24.07.23.
//

import Foundation
import ListOfDatesIOS

final class ButtonPositionGetterSPY: ButtonPositionGetter {
    
    var requestCount: Int {
        getButtonPositionCompletions.count
    }
    
    private var getButtonPositionCompletions: [((CGRect?) -> Void)] = []
    
    func getButtonPosition(name: String, completion: @escaping (CGRect?) -> Void) {
        getButtonPositionCompletions.append(completion)
    }
    
    func completeGetButtonPositionEmpty(at index: Int = 0) {
        getButtonPositionCompletions[index](nil)
    }
    
    func completeGetButtonPosition(with position: CGRect, at index: Int = 0) {
        getButtonPositionCompletions[index](position)
    }
}
