//
//  DatesListIntegrationTests.swift
//  ListOfDatesIOSTests
//
//  Created by Yauheni Zhurauski on 24.07.23.
//

import XCTest
import ListOfDatesIOS

final class DatesListIntegrationTests: XCTestCase {
    
    
    func test_initWithEmptyDatesList() {
        let sut = makeSUT()
        sut.loadViewIfNeeded()
        
        XCTAssertEqual(sut.datesCount, 0)
    }
    
    func test_positionGetter_makesRequestOnInitState() {
        let positionGetter = ButtonPositionGetterSPY()
        let sut = makeSUT(buttonPositionGetter: positionGetter)
        sut.loadViewIfNeeded()
        
        XCTAssertEqual(positionGetter.requestCount, 1)
    }
    
    func test_positionGetter_displaysAddButtonAtDefaultPlaceWhenDeliversEmpty() {
        let positionGetter = ButtonPositionGetterSPY()
        let sut = makeSUT(buttonPositionGetter: positionGetter)
        sut.loadViewIfNeeded()
        
        positionGetter.completeGetButtonPositionEmpty()
        
        XCTAssertEqual(sut.addButtonFrame, .zero)
    }
    
    func test_positionGetter_displaysAddButtonAtSavePlaceWhenDeliverPosition() {
        let positionGetter = ButtonPositionGetterSPY()
        let sut = makeSUT(buttonPositionGetter: positionGetter)
        sut.loadViewIfNeeded()
        
        let MOCKPosition = CGRect(x: 20, y: 20, width: 20, height: 20)
        positionGetter.completeGetButtonPosition(with: MOCKPosition)
        
        XCTAssertEqual(sut.addButtonFrame, MOCKPosition)
    }

    func test_addButton_displaysNewDateAtButtonTap() {
        let sut = makeSUT()
        sut.loadViewIfNeeded()
        
        sut.simulateAddButtonTap()
        
        XCTAssertEqual(sut.datesCount, 1)
    }
    
    private func makeSUT(buttonPositionGetter: ButtonPositionGetter = ButtonPositionGetterSPY(),
                         buttonPositionSaver: ButtonPositionSaver = ButtonPositionSaverSPY(),
                         file: StaticString = #file, line: UInt = #line) -> DatesListViewController {
        let viewController = DatesListComposer().composeWith(buttonPositionGetter: buttonPositionGetter, buttonPositionSaver: buttonPositionSaver)
        
        trackForMemoryLeaks(viewController, file: file, line: line)
        
        return viewController
    }
    
}
