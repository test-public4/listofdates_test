//
//  XCTestCase+Extension.swift
//  ListOfDatesIOSTests
//
//  Created by Yauheni Zhurauski on 24.07.23.
//

import XCTest

extension XCTestCase {
    
    func trackForMemoryLeaks(_ instanse: AnyObject, file: StaticString = #file, line: UInt = #line) {
        addTeardownBlock { [weak instanse] in
            XCTAssertNil(instanse, "Potential memory leak", file: file, line: line)
        }
    }
    
    func anyNSError() -> NSError {
        NSError(domain: "error", code: 0)
    }
    
}
