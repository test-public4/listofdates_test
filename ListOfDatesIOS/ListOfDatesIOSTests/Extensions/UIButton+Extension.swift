//
//  UIButton+Extension.swift
//  ListOfDatesIOSTests
//
//  Created by Yauheni Zhurauski on 24.07.23.
//

import UIKit

extension UIButton {
    func simulateTap() {
        allTargets.forEach { target in
            actions(forTarget: target, forControlEvent: .touchUpInside)?.forEach {
                (target as NSObject).perform(Selector($0))
            }
        }
    }
}
