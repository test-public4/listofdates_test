//
//  AppColors.swift
//  ListOfDatesIOS
//
//  Created by Yauheni Zhurauski on 24.07.23.
//

import UIKit

enum AppColors {
    case gray
    case green
    case darkGreen
    case white
}

extension AppColors {
    var value: UIColor {
        switch self {
        case .gray: return #colorLiteral(red: 0.568627451, green: 0.5568627451, blue: 0.6156862745, alpha: 1)
        case .green: return #colorLiteral(red: 0.7019607843, green: 0.8784313725, blue: 0.7215686275, alpha: 1)
        case .darkGreen: return #colorLiteral(red: 0.652959934, green: 0.7898631328, blue: 0.7215686275, alpha: 1)
        case .white: return #colorLiteral(red: 1, green: 0.9999999404, blue: 0.9999999404, alpha: 1)
        }
    }
}
