//
//  DispatchQueue+Extension.swift
//  ListOfDatesIOS
//
//  Created by Yauheni Zhurauski on 24.07.23.
//

import Foundation

extension DispatchQueue {
    public static func moveToMainQueue(_ function: @escaping ()->(Void)) {
        if Thread.isMainThread {
            function()
        } else {
            DispatchQueue.main.async {
                function()
            }
        }
    }
}
