//
//  MainViewController.swift
//  ListOfDatesIOS
//
//  Created by Yauheni Zhurauski on 24.07.23.
//

import UIKit

public final class MainViewController: NiblessViewController {
    
    private let viewModel: MainViewModel
    
    private let datesListViewControllerFactory: () -> DatesListViewController
    
    public init(viewModel: MainViewModel, datesListViewControllerFactory: @escaping () -> DatesListViewController) {
        self.viewModel = viewModel
        self.datesListViewControllerFactory = datesListViewControllerFactory
        super.init()
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        presentDatesListViewController()
    }
    
    private func presentDatesListViewController() {
        let datesListViewController = datesListViewControllerFactory()
        addFullScreen(childViewController: datesListViewController)
    }
}
