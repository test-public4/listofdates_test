//
//  ButtonPositionSaver.swift
//  ListOfDatesIOS
//
//  Created by Yauheni Zhurauski on 24.07.23.
//

import Foundation

public protocol ButtonPositionSaver {
    func saveButtonPosition(at position: CGRect, name: String, completion: @escaping (Error?) -> Void)
}
