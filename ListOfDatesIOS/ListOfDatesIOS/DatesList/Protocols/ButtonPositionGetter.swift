//
//  ButtonPositionGetter.swift
//  ListOfDatesIOS
//
//  Created by Yauheni Zhurauski on 24.07.23.
//

import Foundation

public protocol ButtonPositionGetter {
    func getButtonPosition(name: String, completion: @escaping (CGRect?) -> Void)
}
