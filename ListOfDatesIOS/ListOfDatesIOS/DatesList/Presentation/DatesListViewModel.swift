//
//  DatesListViewModel.swift
//  ListOfDatesIOS
//
//  Created by Yauheni Zhurauski on 24.07.23.
//

import Foundation
import Combine

public final class DatesListViewModel {
    
    var pickedDates: [Date] = []
    
    private let buttonPositionGetter: ButtonPositionGetter
    private let buttonPositionSaver: ButtonPositionSaver
    
    private (set) var dateWasCreatedAtIndex = PassthroughSubject<Int, Never>()
    private (set) var addButtonSavePosition = PassthroughSubject<CGRect?, Never>()
    private (set) var buttonPositionSaverError = PassthroughSubject<Error?, Never>()
    
    public init(buttonPositionGetter: ButtonPositionGetter, buttonPositionSaver: ButtonPositionSaver) {
        self.buttonPositionGetter = buttonPositionGetter
        self.buttonPositionSaver = buttonPositionSaver
    }
    
    func createDate() {
        let newDate = Date()
        pickedDates.append(newDate)
        
        let newDateIndex = pickedDates.count - 1
        dateWasCreatedAtIndex.send(newDateIndex)
    }
    
    func getButtonPosition(with name: String) {
        buttonPositionGetter.getButtonPosition(name: name) { [weak self] buttonPosition in
            self?.addButtonSavePosition.send(buttonPosition)
        }
    }
    
    func saveButtonPosition(with name: String, position: CGRect) {
        buttonPositionSaver.saveButtonPosition(at: position, name: name) { [weak self] error in
            self?.buttonPositionSaverError.send(error)
        }
    }
}
