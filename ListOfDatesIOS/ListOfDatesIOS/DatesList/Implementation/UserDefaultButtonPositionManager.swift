//
//  UserDefaultButtonPositionManager.swift
//  ListOfDatesIOS
//
//  Created by Yauheni Zhurauski on 24.07.23.
//

import Foundation

public final class UserDefaultButtonPositionManager: ButtonPositionGetter, ButtonPositionSaver {
    
    public init() {}
    
    public func getButtonPosition(name: String, completion: @escaping (CGRect?) -> Void) {
        guard let data = UserDefaults.standard.data(forKey: name) else {
            completion(nil)
            return }
        
        guard let position = try? JSONDecoder().decode(CGRect.self, from: data) else {
            completion(nil)
            return }
        
        completion(position)
    }
    
    public  func saveButtonPosition(at position: CGRect, name: String, completion: @escaping (Error?) -> Void) {
        if let positionData = try? JSONEncoder().encode(position) {
            UserDefaults.standard.set(positionData, forKey: name)
        }
    }
}
