//
//  DatesListComposer.swift
//  ListOfDatesIOS
//
//  Created by Yauheni Zhurauski on 24.07.23.
//

import UIKit

public final class DatesListComposer {
    
    public init() {}
    
    public func composeWith(buttonPositionGetter: ButtonPositionGetter, buttonPositionSaver: ButtonPositionSaver) -> DatesListViewController {
        let viewModel = DatesListViewModel(buttonPositionGetter: buttonPositionGetter, buttonPositionSaver: buttonPositionSaver)
        let viewController = DatesListViewController(viewModel: viewModel)
        
        bindNewDate(on: viewController, with: viewModel)
        bindAddButtonPosition(on: viewController, with: viewModel)
        
        return viewController
    }
    
    private func bindNewDate(on viewController: DatesListViewController, with viewModel: DatesListViewModel) {
        viewModel.dateWasCreatedAtIndex.sink { [weak viewController] newDateIndex in
            self.insertNewDateInTableView(on: viewController, at: newDateIndex)
        }.store(in: &viewController.cancellable)
    }
    
    private func insertNewDateInTableView(on viewController: DatesListViewController?, at index: Int) {
        let insertIndexPath = IndexPath(row: index, section: 0)
        DispatchQueue.moveToMainQueue { [insertIndexPath] in
            viewController?.contentView.datesListTableView.insertRows(at: [insertIndexPath], with: .right)
            viewController?.scrollTableView(at: insertIndexPath)
        }
    }
    
    private func bindAddButtonPosition(on viewController: DatesListViewController, with viewModel: DatesListViewModel) {
        viewModel.addButtonSavePosition.sink { [weak viewController] position in
            guard let position else {
                viewController?.contentView.setupDefaultAddButtonPosition()
                return }
            viewController?.contentView.addButton.frame = position
        }.store(in: &viewController.cancellable)
    }
    
    private func bindButtonsSaverError(on viewController: DatesListViewController, with viewModel: DatesListViewModel) {
        viewModel.buttonPositionSaverError.sink { [self, weak viewController] error in
            guard let error else { return }
            self.displayError(on: viewController, with: error)
        }.store(in: &viewController.cancellable)
    }
    
    private func displayError(on viewController: DatesListViewController?, with error: Error) {
        let title = "Сообщение от системы"
        let message = error.localizedDescription
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Ok", style: .cancel)
        alertController.addAction(okAction)
        
        viewController?.present(alertController, animated: false)
    }
    
}
