//
//  DatesListViewController.swift
//  ListOfDatesIOS
//
//  Created by Yauheni Zhurauski on 24.07.23.
//

import UIKit
import Combine

public final class DatesListViewController: NiblessViewController {
    
    public var contentView: DatesListViewControllerView {
        view as! DatesListViewControllerView
    }
    var cancellable = Set<AnyCancellable>()
    
    private let viewModel: DatesListViewModel
    
    public init(viewModel: DatesListViewModel) {
        self.viewModel = viewModel
        super.init()
    }
    
    public override func loadView() {
        super.loadView()
        view = DatesListViewControllerView()
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        setupDelegates()
        setupTargets()
        
        viewModel.getButtonPosition(with: String(describing: DateListAddButton.self))
    }
    
    func scrollTableView(at indexPath: IndexPath) {
        contentView.datesListTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
    }
    
    private func setupDelegates() {
        contentView.datesListTableView.dataSource = self
        contentView.datesListTableView.delegate = self
    }
    
    private func setupTargets() {
        contentView.addButton.addTarget(self, action: #selector(addButtonWasPressed), for: .touchUpInside)
        
        let longPressedGesture = UILongPressGestureRecognizer(target: self, action: #selector(addButtonWasLongPressed(sender: )))
        contentView.addButton.addGestureRecognizer(longPressedGesture)
    }
    
    
    private func makeVibration() {
        let generator = UIImpactFeedbackGenerator(style: .medium)
        generator.impactOccurred()
    }
    
    private func isButtonInAreayHorizontally(x: CGFloat) -> Bool {
        let leftXLimit = 0 + DatesListViewControllerView.ADD_BUTTON_BORDER / 2
        let rightXLimit = contentView.frame.width - DatesListViewControllerView.ADD_BUTTON_BORDER / 2
        
        guard x >= leftXLimit else { return false }
        guard x <= rightXLimit else { return false }
        
        return true
    }
    
    private func isButtonInAreayVertically(y: CGFloat) -> Bool {
        let topYLimit = 0 + contentView.safeAreaInsets.top + DatesListViewControllerView.ADD_BUTTON_BORDER / 2
        let bottomYLimit = contentView.frame.height - contentView.safeAreaInsets.bottom - DatesListViewControllerView.ADD_BUTTON_BORDER / 2
        
        guard y >= topYLimit else { return false }
        guard y <= bottomYLimit else { return false }
        
        return true
    }
    
    private func moveAddButton(xInArea: Bool, yInArea: Bool, moveView: UIView, location: CGPoint) {
        var newLocation = location
        
        if !xInArea {
            newLocation.x = moveView.frame.midX
        }
        
        if !yInArea {
            newLocation.y = moveView.frame.midY
        }
        
        moveView.center = newLocation
    }
    
}
//MARK: - OBJC Methods
extension DatesListViewController {
    @objc private func addButtonWasPressed() {
        viewModel.createDate()
    }
    
    @objc private func addButtonWasLongPressed(sender: UILongPressGestureRecognizer) {
        guard let viewToMove = sender.view else { return }
        switch sender.state {
        case .began:
            makeVibration()
            UIView.animate(withDuration: 0.2) {
                viewToMove.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            }
        case .changed:
            let location = sender.location(in: contentView)
            
            let xInArea = isButtonInAreayHorizontally(x: location.x)
            let yInArea = isButtonInAreayVertically(y: location.y)
            
            moveAddButton(xInArea: xInArea, yInArea: yInArea, moveView: viewToMove, location: location)
        case .ended:
            UIView.animate(withDuration: 0.2) {
                viewToMove.transform = CGAffineTransform.identity
            }
            viewModel.saveButtonPosition(with: String(describing: DateListAddButton.self), position: contentView.addButton.frame)
        default:
            break
        }
    }
}

extension DatesListViewController: UITableViewDataSource, UITableViewDelegate {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.pickedDates.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: DatesListTableViewCell.self),for: indexPath)
        guard let dateCell = cell as? DatesListTableViewCell else { return cell }
        dateCell.setupCell(with: viewModel.pickedDates[indexPath.row])
        return dateCell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
