//
//  DateListAddButton.swift
//  ListOfDatesIOS
//
//  Created by Yauheni Zhurauski on 24.07.23.
//

import UIKit

public final class DateListAddButton: UIButton {
    
    public override var isHighlighted: Bool {
        didSet {
            let defaultColor = AppColors.green.value
            let highlightedColor = AppColors.darkGreen.value
            
            backgroundColor = isHighlighted ? highlightedColor : defaultColor
        }
    }
}
