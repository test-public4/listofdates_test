//
//  DatesListViewControllerView.swift
//  ListOfDatesIOS
//
//  Created by Yauheni Zhurauski on 24.07.23.
//

import UIKit

public final class DatesListViewControllerView: NiblessView {
    
    static let ADD_BUTTON_BORDER: CGFloat = 60
    
    public var datesListTableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(DatesListTableViewCell.self, forCellReuseIdentifier: String(describing: DatesListTableViewCell.self))
        tableView.backgroundColor = .clear
        tableView.showsVerticalScrollIndicator = false
        return tableView
    }()
    public var addButton: DateListAddButton = {
        let button = DateListAddButton()
        button.backgroundColor = AppColors.green.value
        button.layer.cornerRadius = DatesListViewControllerView.ADD_BUTTON_BORDER / 2
        button.setImage(UIImage(systemName: "plus"), for: .normal)
        return button
    }()
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = AppColors.gray.value
        
        layoutElements()
    }
    
    func setupDefaultAddButtonPosition() {
        addButton.translatesAutoresizingMaskIntoConstraints = false
        layoutAddButton()
    }
    
    private func layoutElements() {
        layoutDatesListTableView()
        addSubview(addButton)
    }
    
    private func layoutDatesListTableView() {
        addSubview(datesListTableView)
        NSLayoutConstraint.activate([
            datesListTableView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            datesListTableView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            datesListTableView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            datesListTableView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor)
        ])
    }
    
    private func layoutAddButton() {
        NSLayoutConstraint.activate([
            addButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            addButton.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -20),
            addButton.widthAnchor.constraint(equalToConstant: DatesListViewControllerView.ADD_BUTTON_BORDER),
            addButton.heightAnchor.constraint(equalToConstant: DatesListViewControllerView.ADD_BUTTON_BORDER)
        ])
    }
}
